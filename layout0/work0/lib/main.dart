import 'package:flutter/material.dart';

Column _buildButtonCol(Color color, IconData icon, String label) {
  return Column(
    mainAxisSize: MainAxisSize.min,
    children: [
      Icon(
        icon,
        color: color,
      ),
      Container(
          child: Text(
        label,
        style:
            TextStyle(fontSize: 12, fontWeight: FontWeight.w400, color: color),
      )),
    ],
  );
}

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Color color = Theme.of(context).primaryColor;
    Widget titleSect = Container(
      padding: const EdgeInsets.all(32),
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: const EdgeInsets.only(bottom: 8),
                  child: Text(
                    'Somewhere lake Campground',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
                Text(
                  'Somewhere, Switzerland',
                  style: TextStyle(color: Colors.grey[500]),
                )
              ],
            ),
          ),
          Icon(
            Icons.star,
            color: Colors.red[500],
          ),
          Text('41')
        ],
      ),
    );
    Widget buttonSect = Container(
        child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        _buildButtonCol(color, Icons.call, 'CALL'),
        _buildButtonCol(color, Icons.near_me, 'ROUTE'),
        _buildButtonCol(color, Icons.share, 'SHARE'),
      ],
    ));
    Widget textSect = Container(
        padding: EdgeInsets.all(32),
        child: Text(
          'Lake Oeschinen lies at the foot of the Blüemlisalp in the Bernese '
          'Alps. Situated 1,578 meters above sea level, it is one of the '
          'larger Alpine Lakes. A gondola ride from Kandersteg, followed by a '
          'half-hour walk through pastures and pine forest, leads you to the '
          'lake, which warms to 20 degrees Celsius in the summer. Activities '
          'enjoyed here include rowing, and riding the summer toboggan run.',
          softWrap: true,
        ));
    return MaterialApp(
      title: 'FlutterLayout Demo',
      home: Scaffold(
        appBar: AppBar(title: const Text('Flutter Layout Demo')),
        body: ListView(
          children: [Image.asset('images/lake.jpg',width: 600,height: 240,fit: BoxFit.fill,),titleSect, buttonSect,textSect],
        ),
      ),
    );
  }
}
